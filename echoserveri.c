#include "csapp.h"
#define MAXARGS 128

void echo(int connfd);
void eval(char *cmdline);
int parseline(char *buf, char **argv);
int builtin_command(char **argv);
char buff[MAXLINE];

void sigchld_handler (int sig){
	while (waitpid(-1,0,WNOHANG)>0)
		;
	return;
}



int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];
	Signal(SIGCHLD,sigchld_handler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		if (Fork()==0){
			Close(listenfd);
			hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
			haddrp = inet_ntoa(clientaddr.sin_addr);
			printf("server connected to %s (%s)\n", hp->h_name, haddrp);

			echo(connfd);
			Close(connfd);
			exit(0);
		}
		/* Determine the domain name and IP address of the client */
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	size_t n;
	rio_t rio;

	Rio_readinitb(&rio, connfd);
	while((n = read(connfd, buff, MAXLINE)) != 0) {
		eval(buff);
		write(connfd, buff, strlen(buff));
	}
}

void eval(char *cmdline)
{
	char *argv[MAXARGS]; /* Argument list execve() */
	char buf[MAXLINE];/* Holds modified command line */
	int bg;/* Should the job run in bg or fg? */
	pid_t pid;/* Process id */
	int ret;
	int status;
	printf("%s", cmdline);
	strcpy(buf, cmdline);
	bg = parseline(buf, argv);
	
	if (argv[0] == NULL)
		return;

	if (!builtin_command(argv)) {
		
		if ((pid = Fork()) == 0) {
			if (execve(argv[0], argv, environ) < 0) {
				ret = raise(SIGABRT);
				printf("%s: Command not found.\n", argv[0]);
				exit(1);
			}
		}
		else{
			waitpid(pid,&status,0);
			printf("status = %d\n",status);
			bzero((char *)&buff,sizeof(buff));
			if (status!=0){
				strcpy(buff, "> ERROR\n");
			}
			else{
				strcpy(buff, "> OK\n");
			}
		}

	}
		//strcpy(buff, "> OK\n");
		return;
}

int builtin_command(char **argv)
{
	if (!strcmp(argv[0], "quit")) /* quit command */
		exit(0);
	if (!strcmp(argv[0], "&"))
		return 1;
	return 0;

}


int parseline(char *buf, char **argv)
{
	char *delim;
	int argc;
	int bg;


	buf[strlen(buf)-1] = ' '; /* Replace trailing ’\n’ with space */
	while (*buf && (*buf == ' ')) /* Ignore leading spaces */
		buf++;

/* Build the argv list */
	argc = 0;
	while ((delim = strchr(buf, ' '))) {
		argv[argc++] = buf;
		*delim = '\0';
		buf = delim + 1;
		while (*buf && (*buf == ' ')) /* Ignore spaces */
			buf++;
	}
	argv[argc] = NULL;

	if (argc == 0)
		return 1;


/* Should the job run in the background? */
	if ((bg = (*argv[argc-1] == '&')) != 0)
		argv[--argc] = NULL;

	return bg;

}
